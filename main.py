"""One Line Drawer

Usage:
    main.py [-h] <filename> [-p Padding] [-c FrameRate] [-g Degree] [-s]

Options:
    -h --help                     Show this screen.
    -p <value> --padding <value>  Brush padding between each row
    -g <value> --gradient <value> Transition degree between the highest and lowest pixel value
    -s --show                     Display Output filtered image.
    -c <value> --capture <value>   Capture animation into frames in ms
"""
import io
import math
import os
import numpy as np

from turtle import *
from PIL import Image
from docopt import docopt


def main():
    arguments = docopt(__doc__, version=1.0)
    # Params
    filename = arguments['<filename>']
    brush_padding = int(arguments['--padding'] or 10)
    gradient = float(arguments['--gradient'] or 2)
    frame_rate = int(arguments['--capture'])
    show = arguments['--show'] or False
    canvas_size = screensize()

    # Setup
    setworldcoordinates(0, canvas_size[1], canvas_size[0], 0)
    setheading(90)

    # Load Image and Process Image
    with Image.open(filename) as image:
        filtered = image
        filtered = filtered.resize((canvas_size[0], int(canvas_size[1] / brush_padding)))
        filtered = filtered.convert('L')
        if show:
            filtered.show()
        image_data = np.array(filtered)
        pass

    # Draw Image
    begin_fill()
    ps_files = []
    count = 0
    # save_timer(frame_rate)
    is_left = True
    for y in range(0, canvas_size[1], brush_padding):
        r = range(canvas_size[0]) if is_left else range(canvas_size[0] - 1, 0, -1)
        for x in r:
            pixel = image_data[int(y / brush_padding), x]
            amplitude = (1 - np.linalg.norm(pixel) / 255) ** gradient
            offset = (brush_padding / 2) * amplitude * math.sin(x)
            target = (x, y + offset)
            goto(target[0], target[1])
            if frame_rate and count % frame_rate == 0:
                ps_files.append(getcanvas().postscript())
            count += 1
        is_left = not is_left

    if frame_rate:
        out_dir = os.getcwd() + '\\.out';
        if not os.path.isdir(out_dir):
            os.mkdir(out_dir)
        name_padding = str(len(str(len(ps_files))))
        for frame, ps in enumerate(ps_files):
            with Image.open(io.BytesIO(ps.encode('utf-8'))) as image:
                image.save(out_dir + '\\' + format(frame, '0>' + name_padding) + '.png')
    end_fill()
    done()
    pass


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()
    pass
